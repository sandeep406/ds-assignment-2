

/*  TODO:  For this problem, build a function called 'hpd' that takes a pointer to a 'struct rht that represents the first node in a linked list.  This function should print all the items in the list in REVERSE order.
Check the header file to find out what the structure definition and function prototype looks like.
*/

#include "header7.h"
#include "stdio.h"
#include "stdlib.h"
#include "stdint.h"
	
void hpd(struct rht *node)
{
   if (node == NULL)
    {
     return;
     }
    hpd(node->next_rht);
    printf("%d\n",node->xlk);
}
