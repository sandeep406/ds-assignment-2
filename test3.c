
/*  TODO:  For this problem, we are going to build a function that creates a linked list node.  Create a function called 'ota' that takes a pointer to a 'struct szb' and an item of type unsigned int.  The function should use malloc to allocate a new linked list node.  The 'struct szb' pointer that was passed will be the pointer to the next node in the list after the newly allocated node.  You also need to initialize the member whb to the other value that was passed.
Check the header file to find out what the structure definition and function prototype looks like.
*/

#include "header3.h"
#include <stdio.h>
#include <stdlib.h>
	
struct szb *ota(struct szb *ptrVar,  unsigned int x)
{	
 struct szb *head = (struct szb *)malloc(sizeof(struct szb));
 head->next_szb = ptrVar;
 head->whb = x;
 return head;
}

