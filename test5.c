
/*  TODO:  For this problem, build a function called 'xwv' that takes a pointer to a 'struct ckc that represents the first node in a linked list.  This function should free all nodes in the list and do so without causing any memory violations.
Check the header file to find out what the structure definition and function prototype looks like.
*/
	
#include "header5.h"
#include "stdio.h"
#include "stdlib.h"

void xwv(struct ckc *node)
{
 struct ckc *temp;
 while(node)
  {	
   temp = node;
   node = node->next_ckc;
   free(temp);
  }
}
