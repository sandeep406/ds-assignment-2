
/*  TODO:  For this problem, build a function called 'smu' that takes two pointers to 'struct gsi's.  These pointers represent the first node in a linked list and they can be null if the list is empty.  The function should determine which linked list is the longer one, and return the pointer to the start of that list.  If they are both the same length, return the first one.
Check the header file to find out what the structure definition and function prototype looks like.
*/

#include "header4.h"
#include<stdlib.h>
#include<stddef.h>	

struct gsi *smu(struct gsi *ptr1, struct gsi *ptr2)
{
 struct gsi *copy1 = ptr1;
 struct gsi *copy2 = ptr2;
 int listA=0;
 int listB=0;
 
  while(ptr1)
  {	
   ptr1 = ptr1->next_gsi;
   listA++;
  }
	
  while(ptr2)
  {	
   ptr2 = ptr2->next_gsi;
   listB++;
  }
	
  if(listA >= listB)
  {
   return copy1;
  }
  else if(listA < listB)
  {
   return copy2 ;
  }
	
  else
  { 
   return 0; 
  }
}
