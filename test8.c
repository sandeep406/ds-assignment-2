
/*  TODO:  For this problem, build a function called 'fzp' that takes a pointer to a pointer to a 'struct gbe' that represents the first node in a linked list, and a second pointer to another linked list.  The function should append the list from the second pointer onto the list specified by the first pointer.
Check the header file to find out what the structure definition and function prototype looks like.
*/

#include "header8.h"
#include "stdio.h"
#include "stdlib.h"

void fzp(struct gbe ** pa, struct gbe * b)
{
  struct gbe *temp = *pa;
  if(*pa == NULL)
   {
    *pa =b;
   }
  else
  {
   while(temp->next_gbe != NULL)
    {
     temp = temp->next_gbe;
    }
   temp->next_gbe = b;
  } 
}
	
