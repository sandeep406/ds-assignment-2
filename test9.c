/*  TODO:  For this problem, build a function called 'lnd' that takes a pointer to a pointer to a 'struct rbl' that represents the first node in a linked list, and a second item that represents the items we want delete.  Return a pointer to a new list where all of the elements in the list that match the second parameter have been deleted.  If the list only contains elements that have the value want to delete, return a null pointer.
Check the header file to find out what the structure definition and function prototype looks like.
*/


#include "header9.h"
#include "stdio.h"
#include "stdlib.h"


struct rbl *lnd(struct rbl *head, unsigned int b)
{
 struct rbl *temp = NULL;
  if(head == NULL)
   {
    return NULL;
   }
  if(head->buf == b)
   {
    temp = head->next_rbl;
    free(head);
    return lnd(temp,b);
   }
 head->next_rbl = lnd(head->next_rbl,b);
 return head;
}
	
